import datetime

from django.test import TestCase
from django.utils import timezone
from django.urls import reverse

from .models import Question


def create_question(question_text, days):
    """
    Create a question with the given `question_text` and published the
    given number of `days` offset to now (negative for questions published
    in the past, positive for questions that have yet to be published).
    """
    time = timezone.now() + datetime.timedelta(days=days)
    return Question.objects.create(question_text=question_text, pub_date=time)


class QuestionModelTests(TestCase):

    def test_was_asked_recently_with_future_question(self):
        """
        was_asked_recently() returns False for questions whose pub_date
        is in the future.
        """
        time = timezone.now() + datetime.timedelta(days=30)
        future_question = Question(pub_date=time)
        self.assertIs(future_question.was_asked_recently(), False)

    def test_was_asked_recently_with_old_question(self):
        """
        was_asked_recently() return False for questions whose pub_date
        is older than one day.
        """
        time = timezone.now() - datetime.timedelta(days=1, seconds=1)
        old_question = Question(pub_date=time)
        self.assertIs(old_question.was_asked_recently(), False)

    def test_was_asked_recently_with_recent_question(self):
        """
        was_asked_recently() return True for questions whose pub_date
        is withing the last day.
        """
        time = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=59)
        recent_question = Question(pub_date=time)
        self.assertIs(recent_question.was_asked_recently(), True)


class QuestionIndexViewTests(TestCase):
    def test_no_question(self):
        """
        If no questions exist, an appropriate message is displayed.
        """
        response = self.client.get(reverse("info:index"))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No questions are available.")
        self.assertQuerysetEqual(response.context['latest_question_list'], [])

    def test_past_question(self):
        """
        Questions with a pub_date in the past are displayed on the
        index page.
        """
        question = create_question(question_text="Past question.", days=-30)
        response = self.client.get(reverse("info:index"))
        self.assertQuerysetEqual(
            response.context['latest_question_list'],
            [question],
        )

    def test_future_question(self):
        """
        Questions with a pub_date in the future aren't displayed on
        the index page.
        """
        create_question(question_text="Future question.", days=30)
        response = self.client.get(reverse("info:index"))
        self.assertContains(response, "No questions are available.")
        self.assertQuerysetEqual(response.context['latest_question_list'], [])

    def test_future_question_and_past_question(self):
        """
        Even if both past and future questions exist, only past questions
        are displayed.
        """
        question = create_question(question_text="Past question.", days=-30)
        create_question(question_text="Future question.", days=30)
        response = self.client.get(reverse("info:index"))
        self.assertQuerysetEqual(
            response.context["latest_question_list"],
            [question],
        )

    def test_two_past_questions(self):
        question1 = create_question(question_text="Past question 1.", days=-30)
        question2 = create_question(question_text="Past question 2.", days=-5)
        response = self.client.get(reverse("info:index"))
        self.assertQuerysetEqual(
            response.context["latest_question_list"],
            [question2, question1],
        )


class QuestionDetailViewTests(TestCase):
    def test_future_question(self):
        """
        The detail view of a question whose pub_date is in the future
        returns a 404 not found.
        """
        future_question = create_question(question_text="Future question.", days=30)
        url = reverse("info:detail", args=(future_question.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_question(self):
        """
        The detail view of a question with a pub_date in the past
        displays the question's text.
        """
        past_question = create_question(question_text="Past question.", days=-30)
        url = reverse("info:detail", args=(past_question.id,))
        response = self.client.get(url)
        self.assertContains(response, past_question.question_text)


class QuestionListTests(TestCase):
    def test_list_of_questions(self):
        """
        The `question_text` is same before and after creating question.
        """
        questions_list = ["What is your name?", "What is your nickname?", "What is your father's name?",
                          "What is your mother's name?"]
        all_created_questions = []
        for question in questions_list:
            each_created_question = create_question(question_text=question, days=0)
            all_created_questions.append(each_created_question)
        questions_list_index = 0
        for created_question in all_created_questions:
            self.assertEqual(created_question.question_text, questions_list[questions_list_index])
            questions_list_index += 1


class AnswerListTests(TestCase):
    """
    `answer_text` for a particular question is same before and after giving answer.
    """
    def test_list_of_answers(self):
        answers_list = ["Fine", "Good", "Not good", "Bad", "Well"]
        created_question = create_question(question_text="How are you?", days=0)
        for answer in answers_list:
            created_question.answer_set.create(answer_text=answer)
        all_answers = []
        for answer in Question.objects.get(pk=created_question.id).answer_set.all():
            all_answers.append(answer.answer_text)
        answers_list_index = 0
        for answer in all_answers:
            self.assertEqual(answer, answers_list[answers_list_index])
            answers_list_index += 1
